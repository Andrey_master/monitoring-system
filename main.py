import tkinter as tk
import logging
from logging import handlers
import requests
import time
import json


class Tele_med():
    def __init__(self):
        # Инициализируем логгер данных:
        self.logger_init()
        self.logger_state.debug('Logger started')

        # Инициализируем данные для доступа по REST API:
        self.my_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2MDdiMjQ1MWUzYWUxZDAwMTBjZGRhNjEiLCJzdWIiOiI2MDQ5MGU4ODUzMDcyZWZjNGJmZDIxOGUiLCJncnAiOiI2MDQ5MGU4ODUzMDcyZWZjNGJmZDIxOGQiLCJsaWMiOmZhbHNlLCJ1c2ciOiJhcGkiLCJmdWxsIjpmYWxzZSwicmlnaHRzIjoxLjUsImlhdCI6MTYxODY4Mjk2MSwiZXhwIjoxNjIxMTk4ODAwfQ.NqI5fwrcu-N6VwAfT-cHEg8x-JRfSV5Dl2i8cWafiDE'
        self.host = 'https://sandbox.rightech.io'
        self.object_id = '607bf5a6e3ae1d0010cde7a2'

        # Инициализиуем шаблон сообщения:
        with open('message_example.json') as json_file:
            self.data = json.load(json_file)

        # Инициализируем окно для графического интерфейса:
        self.window_initialisation()
        self.update_data()
        self.window.mainloop()

    def logger_init(self):
        # Логгер для поступающих данных:
        self.logger_state = logging.getLogger('state_data')
        self.logger_state.setLevel(logging.DEBUG)
        path_to_logs_folder = 'C:/Users/Andrey/Desktop/State_monitoring_system/logs'
        file_handler_state_data = handlers.TimedRotatingFileHandler(path_to_logs_folder + '/state_data.log',
                                                                    when="midnight",
                                                                    interval=1)
        format_log_state_data = logging.Formatter('[%(asctime)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        file_handler_state_data.setFormatter(format_log_state_data)
        file_handler_state_data.suffix = "%Y-%m-%d"
        self.logger_state.addHandler(file_handler_state_data)

    def window_initialisation(self):
        self.window = tk.Tk(screenName='Monitor')
        self.window.winfo_toplevel().title('Monitor')

        self.txt_body_temperature_data = tk.Label(self.window, text='Body temperature:', width=20, height=1)
        self.txt_body_temperature_data.grid(row=0, column=0)
        self.lbl_body_temperature_data = tk.Label(self.window, text='-', fg='black', bg='white', borderwidth=1, relief="solid", width=10, height=1)
        self.lbl_body_temperature_data.grid(row=0, column=1)
        self.txt_body_temperature_data = tk.Label(self.window, text='°C', width=7, height=1)
        self.txt_body_temperature_data.grid(row=0, column=2)

        self.txt_heart_rate_data = tk.Label(self.window, text='Heart rate:', width=20, height=1)
        self.txt_heart_rate_data.grid(row=1, column=0)
        self.lbl_heart_rate_data = tk.Label(self.window, text='-', fg='black', bg='white', borderwidth=1, relief="solid", width=10, height=1)
        self.lbl_heart_rate_data.grid(row=1, column=1)
        self.txt_body_temperature_data = tk.Label(self.window, text='bpm', width=7, height=1)
        self.txt_body_temperature_data.grid(row=1, column=2)

        self.txt_nrv_activity_data = tk.Label(self.window, text='Nrv activity:', width=20, height=1)
        self.txt_nrv_activity_data.grid(row=2, column=0)
        self.lbl_nrv_actiity_data = tk.Label(self.window, text='-', fg='black', bg='white', borderwidth=1, relief="solid", width=10, height=1)
        self.lbl_nrv_actiity_data.grid(row=2, column=1)
        self.txt_body_temperature_data = tk.Label(self.window, text='mV', width=7, height=1)
        self.txt_body_temperature_data.grid(row=2, column=2)

        self.txt_breathing_rate_data = tk.Label(self.window, text='Breathing rate:', width=20, height=1)
        self.txt_breathing_rate_data.grid(row=3, column=0)
        self.lbl_breathing_rate_data = tk.Label(self.window, text='-', fg='black', bg='white', borderwidth=1, relief="solid", width=10, height=1)
        self.lbl_breathing_rate_data.grid(row=3, column=1)
        self.txt_body_temperature_data = tk.Label(self.window, text='Hz', width=7, height=1)
        self.txt_body_temperature_data.grid(row=3, column=2)

        self.txt_pressure_up_data = tk.Label(self.window, text='Pressure SYS:', width=20, height=1)
        self.txt_pressure_up_data.grid(row=4, column=0)
        self.lbl_pressure_up_data = tk.Label(self.window, text='-', fg='black', bg='white', borderwidth=1, relief="solid", width=10, height=1)
        self.lbl_pressure_up_data.grid(row=4, column=1)
        self.txt_body_temperature_data = tk.Label(self.window, text='mm Hg', width=7, height=1)
        self.txt_body_temperature_data.grid(row=4, column=2)

        self.txt_pressure_down_data = tk.Label(self.window, text='Pressure DIA:', width=20, height=1)
        self.txt_pressure_down_data.grid(row=5, column=0)
        self.lbl_pressure_down_data = tk.Label(self.window, text='-', fg='black', bg='white', borderwidth=1, relief="solid", width=10, height=1)
        self.lbl_pressure_down_data.grid(row=5, column=1)
        self.txt_body_temperature_data = tk.Label(self.window, text='mm Hg', width=7, height=1)
        self.txt_body_temperature_data.grid(row=5, column=2)

        self.txt_work_status_data = tk.Label(self.window, text='Work is stopped:', width=20, height=1)
        self.txt_work_status_data.grid(row=6, column=0)
        self.lbl_work_status_data = tk.Label(self.window, text='-', fg='black', bg='white', borderwidth=1, relief="solid", width=10, height=1)
        self.lbl_work_status_data.grid(row=6, column=1)

    def update_data(self):
        self.body_temperature, self.heart_rate, self.nervous_system_activity, self.breathing_rate, self.blood_pressure, self.work_is_stopped = self.obtain_data_from_tele_med()

        self.log_current_state()

        self.lbl_body_temperature_data['text'] = str(self.body_temperature)
        self.lbl_heart_rate_data['text'] = str(self.heart_rate)
        self.lbl_nrv_actiity_data['text'] = str(self.nervous_system_activity)
        self.lbl_breathing_rate_data['text'] = str(self.breathing_rate)
        self.lbl_pressure_up_data['text'] = str(self.blood_pressure[0])
        self.lbl_pressure_down_data['text'] = str(self.blood_pressure[1])
        self.lbl_work_status_data['text'] = str(self.work_is_stopped)

        self.data_analyzer()

        self.window.after(2000, self.update_data)

    def obtain_data_from_tele_med(self):
        """
        Функция делает запрос серверу и получает данные о состоянии объекта мониторинга состояния.

        :param object_id: ID нужного объекта.
        :return: Кортеж из 5 измеряемых параметров и статуса работы.
        """
        # Формируем GET-запрос серверу:
        headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + self.my_token}
        link = self.host + '/api/v1/objects/' + self.object_id
        response = requests.get(link, headers=headers)
        answer_json = response.json()

        # Получаем 5 измеряемых параметров и статус работы:
        body_temperature = answer_json['state']['body_temperature']
        heart_rate = answer_json['state']['heart_rate']
        nervous_system_activity = answer_json['state']['nervous_system_activity']
        breathing_rate = answer_json['state']['breathing_rate']
        blood_pressure = answer_json['state']['blood_pressure'].split(',')
        # Перевод данных давления из строки в список чисел:
        for index, item in enumerate(blood_pressure):
            blood_pressure[index] = int(item)
        work_status = answer_json['state']['work_is_stopped']

        return (body_temperature, heart_rate, nervous_system_activity, breathing_rate, blood_pressure, work_status)

    def data_analyzer(self):
        if ((self.body_temperature > 37.5 or self.heart_rate > 90 or (self.blood_pressure[0] > 140 and self.blood_pressure[1] > 100) or self.breathing_rate > 30) and self.work_is_stopped == False):
            if (self.body_temperature > 37.5):
                self.send_message(message_text='Запрос на остановку работы - высокая температура')
                self.logger_state.warning('Запрос на остановку работы - высокая температура')
            elif (self.heart_rate > 90):
                self.send_message(message_text='Запрос на остановку работы - высокий пульс')
                self.logger_state.warning('Запрос на остановку работы - высокий пульс')
            elif (self.blood_pressure[0] > 140 and self.blood_pressure[1] > 100):
                self.send_message(message_text='Запрос на остановку работы - высокое давление')
                self.logger_state.warning('Запрос на остановку работы - высокое давление')
            elif (self.breathing_rate > 30):
                self.send_message(message_text='Запрос на остановку работы - высокая частота дыхания')
                self.logger_state.warning('Запрос на остановку работы - высокая частота дыхания')

            self.send_work_signal(cmd_identifier='stop_work_signal')

            while (1):
                time.sleep(1)
                self.body_temperature, self.heart_rate, self.nervous_system_activity, self.breathing_rate, self.blood_pressure, self.work_is_stopped = self.obtain_data_from_tele_med()
                self.log_current_state()

                if (self.work_is_stopped == True):
                    break
            self.send_message(message_text='Работа была остановлена')
            self.logger_state.warning('Работа была остановлена')

        elif ((self.body_temperature <= 37.5 and self.heart_rate <= 90 and self.blood_pressure[0] <= 140 and self.blood_pressure[1] <= 100 and self.breathing_rate <= 30) and self.work_is_stopped == True):
            self.send_message(message_text='Работа может быть возобновлена - все показатели в норме')
            self.logger_state.debug('Запрос на возобновление работы - все показатели в норме')
            self.send_work_signal(cmd_identifier='continue_work_signal')

            while (1):
                time.sleep(1)
                self.body_temperature, self.heart_rate, self.nervous_system_activity, self.breathing_rate, self.blood_pressure, self.work_is_stopped = self.obtain_data_from_tele_med()
                self.log_current_state()

                if (self.work_is_stopped == False):
                    break
            self.send_message(message_text='Работа была возобновлена')
            self.logger_state.debug('Работа была возобновлена')

    def send_work_signal(self, cmd_identifier):
        """
        Метод посылает POST-запрос с командой системе мониторинга.

        :param cmd_identifier: Идентификатор команды (строка).
        """
        # Формируем POST-запрос серверу:
        headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + self.my_token}
        link = self.host + '/api/v1/objects/{0}/commands/{1}'.format(self.object_id, cmd_identifier)

        response = requests.post(link, headers=headers)

    def send_message(self, message_text):
        """
        Метод формирует POST-запрос с сообщеним и отправляет его.

        :param message_text: Текст сообщения (строка).
        """
        self.data['time'] = int(time.time() * 1000)
        self.data['_id'] = str(time.time() * 1000)
        self.data['id'] = self.object_id
        self.data['body']['text'] = message_text

        headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + self.my_token}
        link = self.host + '/api/v1/messages'
        response = requests.post(link, headers=headers, data=json.dumps(self.data))

    def log_current_state(self):
        self.logger_state.debug([self.body_temperature, self.heart_rate, self.nervous_system_activity, self.breathing_rate, self.blood_pressure, self.work_is_stopped])


if (__name__ == '__main__'):
    tele_med = Tele_med()
